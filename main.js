var data = {
    "Belarus" : ["Minsk", "Vitebsk", "Mogilev", "Grodno", "Gomel", "Brest"],
    "Russia" : ["Moskow", "S.Petersburg", "Smolensk", "Ufa", "Chita"],
    "Ukrain" : ["Kiev", "Lviv", "Odessa"]
};


$(document).ready(function(){
    var countrySelect = document.getElementById("countries");
    for (var country in data){
        countrySelect.add(new Option(country));
    }

    countrySelect.onchange = populateCities;

    populateCities();

    function populateCities() {
        var citySelect = document.getElementById("cities");

        $(citySelect).empty();

        data[countrySelect.value].forEach(function (city) {
            citySelect.add(new Option(city));
        });
    }

});
